;; -*-lisp-*-
;;
;; my stumpwm init.lisp

(in-package :stumpwm)

;; change the prefix key to something else
(set-prefix-key (kbd "C-t"))

;; prompt the user for an interactive command. The first arg is an
;; optional initial contents.
(defcommand colon1 (&optional (initial "")) (:rest)
  (let ((cmd (read-one-line (current-screen) ": " :initial-input initial)))
    (when cmd
      (eval-command cmd t))))

(define-key *root-map* (kbd "c")   "exec alacritty")
(define-key *root-map* (kbd "C-c") "exec alacritty")
(define-key *root-map* (kbd "l")   "exec rofi -show combi -theme srcery")
(define-key *root-map* (kbd "e")   "exec emacsclient -c")

;; Read some doc
;;(define-key *root-map* (kbd "d") "exec gv")
;; Browse somewhere
;;(define-key *root-map* (kbd "b") "exec waterfox")
;; Ssh somewhere
;;(define-key *root-map* (kbd "C-s") "colon1 exec xterm -e ssh ")
;; Lock screen
;;(define-key *root-map* (kbd "C-l") "exec xlock")

;; Web jump (works for DuckDuckGo and Imdb)
;;(defmacro make-web-jump (name prefix)
;;  `(defcommand ,(intern name) (search) ((:rest ,(concatenate 'string name " search: ")))
;;    (nsubstitute #\+ #\Space search)
;;    (run-shell-command (concatenate 'string ,prefix search))))

;;(make-web-jump "duckduckgo" "firefox https://duckduckgo.com/?q=")
;;(make-web-jump "imdb" "firefox http://www.imdb.com/find?q=")

;; C-t M-s is a terrble binding, but you get the idea.
;;(define-key *root-map* (kbd "M-s") "duckduckgo")
;;(define-key *root-map* (kbd "i") "imdb")

(load-module "ttf-fonts")
;; Message window font
(set-font (make-instance 'xft:font :family "BlexMono Nerd Font Mono" :subfamily "Bold" :size 10.5))

;; mode line
(setf *default-group-name*         "sys"
      *mode-line-background-color* "#1C1B19"
      *mode-line-foreground-color* "#BAA67F"
      *mode-line-border-color*     "#444444"
      *mode-line-timout* 2
      ;; formatting
      *time-format-string-default* "%a %e. %b %H:%M"
      *time-modeline-string*       "%a %e. %b %H:%M"
      *window-format*              " %n%s %33t "
      *group-format*               "%n: %t"
      *screen-mode-line-format*    "[ ^B%g^b ] %W ^> %d")
(toggle-mode-line (current-screen) (current-head))

;; junk to make things more usable
(setf *mouse-focus-policy*           :click
      *window-border-style*          :tight
      *normal-border-width*    2
      *maxsize-border-width*   2
      *transient-border-width* 2
      *message-window-gravity*       :top
      *message-window-input-gravity* :top
      *input-window-gravity*         :top
      *timeout-wait* 3)
(set-normal-gravity :center)
(set-fg-color "#FCE8C3")
(set-bg-color "#1C1B19")
(set-msg-border-width 2)

(set-border-color        "#BAA67F")
(set-focus-color         "#BAA67F")
(set-unfocus-color       "#121212")
(set-float-focus-color   "#BAA67F")
(set-float-unfocus-color "#121212")

;;; Define window placement policy...

;; Clear rules
(clear-window-placement-rules)

;; Last rule to match takes precedence!
;; TIP: if the argument to :title or :role begins with an ellipsis, a substring
;; match is performed.
;; TIP: if the :create flag is set then a missing group will be created and
;; restored from *data-dir*/create file.
;; TIP: if the :restore flag is set then group dump is restored even for an
;; existing group using *data-dir*/restore file.

(define-frame-preference "web"
    (0 nil t :create t :role "browser"))
(define-frame-preference "msg"
    (0 nil t :create t :class "discord"))
(define-frame-preference "games"
    (0 nil t :create t :class "Steam"))

(load-module "swm-gaps")

;; Head gaps run along the 4 borders of the monitor(s)
(setf swm-gaps:*head-gaps-size* 4)

;; Inner gaps run along all the 4 borders of a window
(setf swm-gaps:*inner-gaps-size* 2)

;; Outer gaps add more padding to the outermost borders of a window (touching
;; the screen border)
(setf swm-gaps:*outer-gaps-size* 4)

(load-module "screenshot")

(load-module "kbd-layouts")
(kbd-layouts:keyboard-layout-list "us -variant altgr-intl" "ru -variant phonetic")

(load-module "end-session")
;; autorun stuff

;; swank lol
(require :swank)
(swank-loader:init)
(swank:create-server :port 4004
                     :style :SPAWN
                     :dont-close t)

(run-commands
 "exec pipewire"
 "exec easyeffects --gapplication-service"
 "exec picom"
 "exec dunst"
 "exec nitrogen --restore"
 "exec emacs --daemon"
 "exec xsetroot -cursor_name left_ptr")

