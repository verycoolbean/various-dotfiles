;; indentation
(setq tab-width 4) ; or any other preferred value
(defvaralias 'c-basic-offset 'tab-width)
(defvaralias 'cperl-indent-level 'tab-width)

;; splash screen be gone
(setq inhibit-startup-message t)
(setq initial-scratch-message nil)

;; make emacs a little less ugly
(scroll-bar-mode -1)
(tool-bar-mode -1)
(tooltip-mode -1)
(set-fringe-mode 10)
(menu-bar-mode -1)
(setq visible-bell nil)
(setq frame-resize-pixelwise t)
(setq org-hide-emphasis-markers t)

;; font
(set-face-attribute 'default nil :font "Spleen")

;; enable package repos
(require 'package)
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
			 ("org" . "https://orgmode.org/elpa/")
			 ("elpa" . "https://elpa.gnu.org/packages/")))
(package-initialize)

;; use-package is pretty neat i think
(unless (package-installed-p 'use-package)
  (package-install 'use-package))
(require 'use-package)
(setq use-package-always-ensure t)

;; theme
(use-package srcery-theme
  :config
  (load-theme 'srcery t))

(defun clienttheme (frame)
  (select-frame frame)
  (load-theme 'srcery t)
  (set-face-attribute 'default nil :font "BlexMono Nerd Font Mono" :height 100))

(if (daemonp)
    (add-hook 'after-make-frame-functions #'clienttheme)
  (load-theme 'srcery t)
  (set-face-attribute 'default nil :font "BlexMono Nerd Font Mono" :height 100))

;; modeline
(use-package feebleline
  :ensure t
  :config (setq feebleline-msg-functions
		'((feebleline-line-number         :post "" :fmt "%5s")
		  (feebleline-column-number       :pre ":" :fmt "%-2s")
		  (feebleline-file-directory      :face feebleline-dir-face :post "")
		  (feebleline-file-or-buffer-name :face font-lock-keyword-face :post "")
		  (feebleline-file-modified-star  :face font-lock-warning-face :post "")
		  (feebleline-git-branch          :face feebleline-git-face :pre " : ")
		  (feebleline-project-name        :align right)))
  (feebleline-mode 1))

;; completions
(use-package vertico
  :init
  (vertico-mode))

;;keybinds
(use-package meow
  :init
  ;; set up keybinds
  (defun meow-setup ()
	(setq meow-cheatsheet-layout meow-cheatsheet-layout-colemak)
	(meow-motion-overwrite-define-key
	 ;; Use e to move up, n to move down.
	 ;; Since special modes usually use n to move down, we only overwrite e here.
	 '("o" . meow-prev)
	 '("e" . meow-next)
	 '("<escape>" . ignore))
	(meow-leader-define-key
	 '("?" . meow-cheatsheet)
	 ;; To execute the originally e in MOTION state, use SPC e.
	 '("e" . "H-e")
	 '("1" . meow-digit-argument)
	 '("2" . meow-digit-argument)
	 '("3" . meow-digit-argument)
	 '("4" . meow-digit-argument)
	 '("5" . meow-digit-argument)
	 '("6" . meow-digit-argument)
	 '("7" . meow-digit-argument)
	 '("8" . meow-digit-argument)
	 '("9" . meow-digit-argument)
	 '("0" . meow-digit-argument))
	(meow-normal-define-key
	 '("0" . meow-expand-0)
	 '("1" . meow-expand-1)
	 '("2" . meow-expand-2)
	 '("3" . meow-expand-3)
	 '("4" . meow-expand-4)
	 '("5" . meow-expand-5)
	 '("6" . meow-expand-6)
	 '("7" . meow-expand-7)
	 '("8" . meow-expand-8)
	 '("9" . meow-expand-9)
	 '("-" . negative-argument)
	 '(";" . meow-reverse)
	 '("," . meow-inner-of-thing)
	 '("." . meow-bounds-of-thing)
	 '("[" . meow-beginning-of-thing)
	 '("]" . meow-end-of-thing)
	 '("/" . meow-visit)
	 '("n" . meow-append)
	 '("N" . meow-open-below)
	 '("b" . meow-back-word)
	 '("B" . meow-back-symbol)
	 '("c" . meow-change)
	 '("d" . meow-delete)
	 '("o" . meow-prev)
	 '("O" . meow-prev-expand)
	 '("f" . meow-find)
	 '("g" . meow-cancel-selection)
	 '("G" . meow-grab)
	 '("i" . meow-left)
	 '("I" . meow-left-expand)
	 '("a" . meow-right)
	 '("A" . meow-right-expand)
	 '("j" . meow-join)
	 '("k" . meow-kill)
	 '("l" . meow-line)
	 '("L" . meow-goto-line)
	 '("m" . meow-mark-word)
	 '("M" . meow-mark-symbol)
	 '("e" . meow-next)
	 '("E" . meow-next-expand)
	 '("d" . meow-block)
	 '("D" . meow-to-block)
	 '("p" . meow-yank)
	 '("q" . meow-quit)
	 '("r" . meow-replace)
	 '("s" . meow-insert)
	 '("S" . meow-open-above)
	 '("t" . meow-till)
	 '("u" . meow-undo)
	 '("U" . meow-undo-in-selection)
	 '("v" . meow-search)
	 '("w" . meow-next-word)
	 '("W" . meow-next-symbol)
	 '("x" . meow-delete)
	 '("X" . meow-backward-delete)
	 '("y" . meow-save)
	 '("z" . meow-pop-selection)
	 '("'" . repeat)
	 '("<escape>" . ignore)))
  :config
  (require 'meow)
  (meow-setup)
  (meow-global-mode 1))

(use-package yaml-mode)
(use-package paren-face
  :hook (prog-mode . paren-face-mode))
(use-package slime
  :config
  (load (expand-file-name "~/.quicklisp/slime-helper.el"))
  (setq inferior-lisp-program "sbcl"))
(use-package lua-mode)
(use-package magit)

;; shit it does automatically, just dont look at it its technically my fault but i deny it
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(magit ayu-theme lua-mode vertico use-package meow srcery-theme ##)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
