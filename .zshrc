## beany config!

# completion stuff
zstyle :compinstall filename '/home/bean/.zshrc'

autoload -Uz compinit
compinit

# plugins
source .antigen/antigen.zsh
antigen bundle zdharma-continuum/fast-syntax-highlighting
antigen bundle Aloxaf/fzf-tab
antigen bundle clarketm/zsh-completions
antigen apply

# aliases
alias ls="ls --color=auto"
alias la="ls -A"
alias ll="ls -l"
alias lla="ls -l -A"

# smol shit i will never touch again
HISTFILE=~/.histfile
HISTSIZE=500
SAVEHIST=2000
setopt extendedglob nomatch notify
bindkey -e

# prompt
source .antigen/beany-prompt.zsh

# luarocks
export PATH=$PATH:~/.luarocks/bin:~/.luarocks51/bin:~/.local/bin
export LUA_CPATH=";;${HOME}/.luarocks/lib/lua/5.1/?.so"
export LUA_PATH=";;${HOME}/.luarocks/share/lua/5.1/?.lua;${HOME}/.luarocks/share/lua/5.1/?/init.lua"

export LUA_CPATH_5_3=";;${HOME}/.luarocks/lib/lua/5.3/?.so"
export LUA_PATH_5_3=";;${HOME}/.luarocks/share/lua/5.3/?.lua;${HOME}/.luarocks/share/lua/5.3/?/init.lua"
