#!/usr/bin/zsh

######################################################################################
# a pretty cool prompt, thats also really minimal and thusly immune to all criticism #
#                                                                                    #
# a main goal was to make it not break when resizing the terminal like literally     #
# every fucking multi-line prompt on earth... by keeping it single line :)           #
######################################################################################


autoload -U colors && colors
# PS1 & PS2 near identical to make sure they're in line
#  doesnt matter and noone uses PS2, but i like it
# the way it prints the cwd was taken from some stackoverflow post i sadly closed sry
PROMPT="%F{blue}%(5~|%-1~/.../%3~|%4~)%F{reset_color} * "
PROMPT2="%F{blue}%(5~|%-1~/.../%3~|%4~)%F{reset_color} ❯ "

returncode () {
if (( $1 > 1 )); then
   	echo "SIG$(kill -l $1)"
else
    echo $1
fi
}
RPROMPT="%(?..%F{red}[$(returncode $?)]%F{reset_color%}"
